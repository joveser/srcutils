#!/usr/bin/env python
# -*- coding: ascii -*-


class CppCommentLexer:
    """Lexical scanner class for extracting comments from C++ code.
    
    V1.0 - 2012-10-10 by Johannes Veser <jveser@gmx.de>
    
    Usage through the iterator protocol.
    
    Methods for the caller:
    
     - __init__(input) # Constructor: accepts any string or a file like object.
     - __iter__()      # Iterator protocol method: returns self
     - next()          # Iterator protocol method: returns (tokenId, tokenText)
    
    The most important method is `next()`. It returns a 2-tuple consisting of
    token ID and token text.
    
    The scanner only distinguishes between the following token/block types
    needed to correctly recognize comment regions:
    
    - t_BLK_COMMENT     # block comment
    - t_LNE_COMMENT     # line comment
    - t_STR_LITERAL     # string literal
    - t_CHR_LITERAL     # character literal
    - t_CODE            # code region
    
    Code regions cover everything not falling into another category, including
    whitespace, preprocessor directives and any c/c++ code.
    
    This class is self contained, so you can copy the class as is into other
    python code.
    """
    
    # Token-IDs for all known token types.
    t_BLK_COMMENT   = 1 # /*...*/
    t_LNE_COMMENT   = 2 # //...
    t_STR_LITERAL   = 3 # ".."
    t_CHR_LITERAL   = 4 # '..'
    t_CODE          = 5 # any other code
    # Compiler directives are included in t_CODE as some of them can contain
    # string literals. By covering directives with t_CODE those string literals
    # are simply handled by t_STR_LITERAL/t_CHR_LITERAL handling code with no
    # additional effort.
    
    class Stream:
        """Helper class for convenient access to (unicode) data."""
        
        def __init__(self, data):
            self.data = data
            self.len = len(self.data)
            self.pos = 0
        
        def getNext(self):
            if self.pos >= self.len:
                return None
            else:
                c = self.data[self.pos]
                self.pos = self.pos + 1
                return c
        
        def advance(self):
            self.pos = self.pos + 1
        
        def peek0(self):
            if self.pos >= self.len:
                return None
            else:
                return self.data[self.pos]
        
        def peek1(self):
            if self.pos + 1 >= self.len:
                return None
            else:
                return self.data[self.pos + 1]
    
    class NormalizingStream:
        """Class for automatic conversion of trigraphs and CR-LF."""
    
        def __init__(self, inputStream):
            self.input = inputStream
            self.c = self.produceChar()
        
        def __iter__(self):
            return self
        
        def next(self):
            c = self.getNext()
            if c is None:
                raise StopIteration
            return c
        
        def getNext(self):
            # next char is already in self.c
            c = self.c
            if c is None:
                return c
            
            c2 = self.produceChar()
            if c == u'\\' and c2 == u'\n': # continuation line?
                # get char after continuation line
                c = self.produceChar()
                if c is None:
                    return c
                c2 = self.produceChar()
            
            self.c = c2 # prepare char for next call
            return c # return current char
        
        def produceChar(self):
            """Returns the next char or None if no more input is left.
            Converts CR-LF to LF; replaces trigraphs."""
        
            c = self.input.getNext()
            
            if c == u'\r':
                # CR-LF conversion
                if self.input.peek0() == u'\n':
                    self.input.advance()
                    c = u'\n' # CR-LF is replaced with LF
            elif c == u'?':
                # trigraph conversion
                t = None
                if self.input.peek0() == u'?':
                    c2 = self.input.peek1()
                    if c2 == u'=':
                        t = u'#'
                    elif c2 == u'/':
                        t = u'\\'
                    elif c2 == u"'":
                        t = u'^'
                    elif c2 == u'(':
                        t = u'['
                    elif c2 == u')':
                        t = u']'
                    elif c2 == u'!':
                        t = u'|'
                    elif c2 == u'<':
                        t = u'{'
                    elif c2 == u'>':
                        t = u'}'
                    elif c2 == u'-':
                        t = u'~'
                if not t is None:
                    self.advance()
                    self.advance()
                    c = t
            return c
    
    class TokenBuffer:
        """Helper class for keeping read buffer state."""
    
        def __init__(self, input):
            self.input = input
            self.buf = []
            self.unfed = []
        
        def hasZeroLength(self):
            return not self.buf and not self.unfed
        
        def get(self, pos):
            while len(self.buf) <= pos:
                self.buf.append(self.nextChar())
            return self.buf[pos]
        
        def isTextEmpty(self):
            return not self.buf
        
        def text(self):
            return ''.join(self.buf)
        
        def nextChar(self):
            if self.unfed:
                return self.unfed.pop()
            else:
                return self.input.next()
        
        def unfeed(self):
            if self.buf:
                self.unfed.append(self.buf.pop())
        
        def clear(self):
            buf = self.unfed
            self.unfed = []
            buf.reverse()
            self.buf = buf
    
    #=========================================================================
    
    def __init__(self, input):
        """Creates a CppCommentLexer instance for the given input.
        
        `input` may be any string or a file like object (anything providing a
        "read()" method returning the whole content as a string).
        """
        self.input = self.prepareInput(input)
        self.buf = self.TokenBuffer(self.input)
        # On start we assume t_CODE (see below)
        self.tokId = self.t_CODE
    
    def __iter__(self):
        return self
    
    def next(self):
        """The primary method for callers. Part of the iterator protocol.
        
        Returns a 2-Tuple with token-ID and token text. For valid token-IDs
        see the t_??? class constants. Raises StopIteration after all input has
        been processed.
        """
        return self.produceToken()
    
    def produceToken(self):
        if self.tokId is None:
            raise StopIteration
        tokId = self.tokId
        self.tokId = None
        newTokId = None
        # After any non-code tokens we always assume that a new t_CODE token
        # starts. Empty t_CODE blocks are skipped. This allows relatively
        # efficient parsing and at the same time keeps the recognition logic
        # in the consume...-methods simple.
        try:
            if tokId == self.t_CODE:
                newTokId = self.consumeCode()
                # 
                if self.buf.isTextEmpty():
                    tokId = newTokId
                    newTokId = self.consumeNonCode(tokId)
            else:
                newTokId = self.consumeNonCode(tokId)
        except StopIteration: # Raised by input when no input left.
            # No input left and buffer empty?
            if self.buf.hasZeroLength():
                raise StopIteration # Final stop.
        text = self.buf.text()
        self.buf.clear()
        self.tokId = newTokId # May be None.
        return tokId, text
    
    def consumeNonCode(self, tokId):
        result = None
        if tokId == self.t_BLK_COMMENT:
            result = self.consumeBlkComment()
        elif tokId == self.t_LNE_COMMENT:
            result = self.consumeLneComment()
        elif tokId == self.t_STR_LITERAL:
            result = self.consumeStrLiteral()
        elif tokId == self.t_CHR_LITERAL:
            result = self.consumeChrLiteral()
        return result
    
    def consumeCode(self):
        """Consumes all input until the start of a non-code token is
        encountered. Here code includes compiler directives, whitespace, all
        definitions, statements etc. i.e. everything excluding comments and
        string or character literals.
        """
        buf = self.buf
        tokId = None
        pos = 0 # Other may be zero length.
        while 1:
            c = buf.get(pos)
            if c == u'"':
                tokId = self.t_STR_LITERAL
                buf.unfeed()
                break
            elif c == u"'":
                tokId = self.t_CHR_LITERAL
                buf.unfeed()
                break
            elif c == u'/':
                # Look ahead.
                c2 = buf.get(pos+1)
                # (If no match for pos+1: reprocess buf.get(pos) in next loop.)
                if c2 == u'*':
                    tokId = self.t_BLK_COMMENT
                    buf.unfeed()
                    buf.unfeed()
                    break;
                elif c2 == u'/':
                    tokId = self.t_LNE_COMMENT
                    buf.unfeed()
                    buf.unfeed()
                    break;
            pos = pos + 1
        return tokId
    
    def consumeBlkComment(self):
        """Consumes all input until the end of the block comment."""
        buf = self.buf
        pos = 3 # min index for closing / (/**/ <- 3)
        while 1:
            c = buf.get(pos) # possibly raises StopIteration
            if c == u'/':
                if buf.get(pos-1) == u'*':
                    break;
            pos = pos + 1
        # By default assume t_CODE.
        return self.t_CODE
    
    def consumeLneComment(self):
        """Consumes all input until the end of the line comment."""
        buf = self.buf
        pos = 2 # min index for closing newline
        while 1:
            c = buf.get(pos) # possibly raises StopIteration
            if c == u'\n':
                 break;
            pos = pos + 1
        # By default assume t_CODE.
        return self.t_CODE
    
    def consumeStrLiteral(self):
        """Consumes all input until the end of the string literal."""
        buf = self.buf
        pos = 1
        while 1:
            c = buf.get(pos) # possibly raises StopIteration
            if c == '"':
                # Quote preceded by a backslash?
                if buf.get(pos-1) == u'\\':
                    # The quote closes the string if the backslash is escaped.
                    if pos >= 3 and buf.get(pos-2) == u'\\': # 3 bec. buf[0]=="
                        break
                else: # non-escaped quote
                    break
            elif c == u'\n': # newline also ends a string literal
                buf.unfeed() # don't include the newline
                break
            pos = pos + 1
        # By default assume t_CODE.
        return self.t_CODE
    
    def consumeChrLiteral(self):
        """Consumes all input until the end of the character literal."""
        buf = self.buf
        pos = 1
        while 1:
            c = buf.get(pos) # possibly raises StopIteration
            if c == u"'":
                # Quote preceded by a backslash?
                if buf.get(pos-1) == u'\\':
                    # The quote closes the string if the backslash is escaped.
                    if pos >= 3 and buf.get(pos-2) == u'\\': # 3 bec. buf[0]=='
                        break
                else: # non-escaped quote
                    break
            elif c == u'\n': # newline also ends char literal
                buf.unfeed() # don't include the newline
                break
            pos = pos + 1
        # By default assume t_CODE.
        return self.t_CODE
    
    def prepareInput(self, input):
        data = input
        if isinstance(data, unicode):
            pass
        elif isinstance(data, str):
            data = unicode(data)
        else:
            data = data.read() # try to read file
            if isinstance(data, str):
                data = unicode(data)
        stream = self.Stream(data)
        stream = self.NormalizingStream(stream)
        return stream


def test():
    """Basic testing function for class CppCommentLexer."""
    import codecs
    # Test source code:
    source_1 = r"""/**    \file

    \brief Implementation of rounding functions.

    :date       :ver        :author      :comment
    2003-09-30  1.00         J.Doe       Created.
*/

#include "round.h"

/**
    \param f Number to be rounded (float)
    \param decimals Number of decimals to round to.
*/
float round(float f, int decimals)
{
    if(decimals <= 0)
        return round(f);
    int q = 10;
    for(int i = 1; i < decimals; ++i)
        q *= 10;
    return (float) round((float) q * f) / (float)q;
}
"""
    
    source_1_wanted_result = [
        (1, u'/**    \\file\n\n    \\brief Implementation of rounding functions.\n\n    :date       :ver        :author      :comment\n    2003-09-30  1.00         J.Doe       Created.\n*/'),
        (5, u'\n\n#include '), (3, u'"round.h"'), (5, u'\n\n'), (1, u'/**\n    \\param f Number to be rounded (float)\n    \\param decimals Number of decimals to round to.\n*/'),
        (5, u'\nfloat round(float f, int decimals)\n{\n    if(decimals <= 0)\n        return round(f);\n    int q = 10;\n    for(int i = 1; i < decimals; ++i)\n        q *= 10;\n    return (float) round((float) q * f) / (float)q;\n}\n')
    ]
    
    lexer = CppCommentLexer(source_1)
    source_1_result = list(lexer)
    if source_1_result != source_1_wanted_result:
        print "ERROR: 1/1 test cases failed!"
    else:
        print "SUCCESS: 1/1 test cases successful."


# Main program
if __name__ == "__main__":
    test()
